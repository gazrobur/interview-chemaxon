const fsPromises = require('fs').promises;
const createReadStream = require('fs').createReadStream;
const path = require('path');
const formidable = require('formidable');
const AWS = require('aws-sdk');

AWS.config.update({
    accessKeyId: "AKIARXXVC57WS3SO3FF3",
    secretAccessKey: "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",
});

const s3 = new AWS.S3();

const uploadFiles = async req => {
    const form = formidable({
        multiples: true,
        maxFileSize: 3 * 1024 * 1024,
        uploadDir: `${__dirname}/../uploads`,
        keepExtensions: true,
    });

    return new Promise((resolve, reject) => {
        form.parse(req)
            .on('file', (name, file) => {
                fsPromises.rename(file.filepath, path.join(form.uploadDir, file.originalFilename));
                uploadFilesIntoAWS(form, file);
            })
            .on('error', err => {
                reject({ success: false, error: err });
            })
            .on('end', () => {
                resolve({ success: true, msg: 'File successfully uploaded!' });
            });
    });
}

const uploadFilesIntoAWS = (form, file) => {

    const params = {
        Bucket: 'interview-chemaxon',
        Key: file.originalFilename,
        Body: createReadStream(path.join(form.uploadDir, file.originalFilename)),
    };

    s3.putObject(params, (err, res) => {
        if (err) {
            console.log("Error uploading data: ", err);
        } else {
            // console.log("Successfully uploaded data", res);
        }
    });
}

const listUserFiles = uid => {
    try {
        return fsPromises.readdir(`./uploads/${uid}`);
    } catch (err) {
        console.error('Error occured while reading directory!', err);
    }
}

const listAWSFiles = async () => {

    const params = {
        Bucket: 'interview-chemaxon',
    }

    return new Promise((resolve, reject) => {
        s3.listObjects(params, function (err, data) {
            if(err){
                console.log('Failed to list files', err);
                reject(err);
            };

            resolve(data.Contents);
        });
    });
}

const signedUrl = async (fileName) => {
    AWS.config.update({
        signatureVersion: 'v4',
        region: 'us-east-2'
    });

    const s3 = new AWS.S3();

    const params = {
        Bucket: 'interview-chemaxon',
        Key: fileName,
        Expires: 60
    }

    try {
        return await s3.getSignedUrlPromise('getObject', params);
    } catch (err) {
        console.log('Failed to get signed url', err);
    }
}

const listFiles = () => {
    try {
        return fsPromises.readdir(`./uploads`, {
            encoding: 'buffer'
        });
    } catch (err) {
        console.error('Error occured while reading directory!', err);
    }
}

const deleteFile = fileName => {
    try {
        deleteFileFromAWS(fileName);
        return fsPromises.unlink(`${__dirname}/../uploads/${fileName}`);
    } catch (err) {
        console.error('Error occured while deleting the file!', err);
    }
}

const deleteFileFromAWS = (fileName) => {

    const params = {
        Bucket: 'interview-chemaxon',
        Key: fileName,
    };

    s3.deleteObject(params, (err, res) => {
        if (err) {
            console.log("Error deleting data: ", err);
        } else {
            console.log("Successfully deleted data", res);
        }
    });
}

module.exports = {
    signedUrl,
    deleteFile,
    deleteFileFromAWS,
    listFiles,
    listAWSFiles,
    listUserFiles,
    uploadFiles,
    uploadFilesIntoAWS,
}