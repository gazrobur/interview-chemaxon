const express = require('express');

const { listUserFiles, deleteFile, uploadFiles, listAWSFiles, signedUrl } = require('./files-operations');

const app = express();
const cors = require('cors');
const port = 3000;

app.use(cors());
app.use(express.json());


app.post('/api/signed', async (req, res) => {
  const { fileName } = req.body;
  try {
    const url = await signedUrl(fileName);
    res.send({ signedUrl: url });
  } catch (error) {
    res.status(404).send('Bad request!');
  }
});

app.get('/api/files/:uid', async (req, res) => {
  const { uid } = req.params;
  try {
    const files = await listUserFiles(uid);
    res.send({ userFiles: files });
  } catch (error) {
    res.status(404).send('Bad request!');
  }
});

app.post('/api/files', async (req, res) => {
  try {
    const upload = await uploadFiles(req);
    res.send(upload);
  } catch (error) {
    res.status(404).send(error);
  }
});

app.get('/api/files', async (req, res) => {
  try {
    const files = await listAWSFiles();
    res.send(files);
  } catch (error) {
    res.status(404).send({ success: false, error });
  }
});

app.delete('/api/files', async (req, res) => {
  const { fileName } = req.body;
  try {
    await deleteFile(fileName);
    res.send({ success: true, msg: 'File successfully deleted!'});
  } catch (error) {
    res.status(404).send({ success: false, error });
  }
});


module.exports = app.listen(port, async () => {
  console.log(`App listening at http://localhost:${port}`);
});