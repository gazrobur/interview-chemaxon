## **Installing**

---

Using npm to install the required packages:

```console
$ npm i
```

## **Testing**

---
You can run tests in order to check algorithm's result.
```console
npm test
```

## **Running the application**

---

### Development
Listening on port 3000 and watching for further changes in the code. 

```console
npm run start:dev
```

### Production
If you want to run the applicaiton in production mode

```console
npm start
```

---

## **License**

[MIT](LICENSE)