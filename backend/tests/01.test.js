const app = require('../src/index');
const chai = require('chai');
const chaiHttp = require('chai-http');
const fsPromises = require('fs').promises;

const { expect } = chai;
chai.use(chaiHttp);


describe('File operations', function () {
    it('should fail to upload a file', async () => {
        const fileBuffer = await fsPromises.readFile(`${__dirname}/10m.jpg`);
        const response = await chai.request(app)
            .post('/api/files')
            .attach('file', fileBuffer, '10m.jpg')
            expect(response.status).to.equal(404);
    });

    it('should upload a file', async () => {
        const fileBuffer = await fsPromises.readFile(`${__dirname}/chemaxon.jpg`);
        const response = await chai.request(app)
            .post('/api/files')
            .attach('file', fileBuffer, 'ch-test.jpg')
            expect(response.status).to.equal(200);
    });

    it('should retrieve uploaded file', async () => {
        // Adding setTimeout because of the speed of the upload
        await new Promise(resolve => setTimeout(resolve, 2000));
        const response = await chai.request(app)
            .get('/api/files')
        const uploadedFiles = response.body;
        const filteredFile = uploadedFiles.filter(file => file.Key === 'ch-test.jpg');
        expect(filteredFile[0].Key).to.equal('ch-test.jpg');
    }).timeout(6000);

    it('should delete uploaded file', async () => {
        const response = await chai.request(app)
            .delete('/api/files')
            .send({fileName: 'ch-test.jpg'});
        expect(response.body.success).to.equal(true);
    });
})