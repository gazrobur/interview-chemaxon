import React, { Component } from 'react';

import './FileUpload.css';
import '../../index.css';


class FileUpload extends Component {

    apiURL = `http://localhost:3000/api`;

    constructor(props) {
        super(props);
        this.state = {
            fileState: null,
            error: null,
            msg: null
        }
    }

    handleFilesState = (e) => {
        let files = e.target.files;
        this.setState({ fileState: files })
    }

    handleSubmitFile = () => {
        if (this.state.fileState !== null){
            let formData = new FormData();
            for (let i = 0; i < this.state.fileState.length; ++i) {
                formData.append('file', this.state.fileState[i]);
            }

            fetch(`${this.apiURL}/files`, {
                method: 'POST',
                body: formData,
            })
            .then(res => res.json())
            .then(json => {
                if (json.success === false) {
                    this.setState({ error: json.error.code })
                } else {
                    this.setState({ msg: json.msg });
                }
            });
        }
    }

    render() { 
        return (
            <div className="FileUpload">
                <div className="FileUpload-container">
                    <input className="FileUpload-input-file" type="file" onChange={this.handleFilesState} multiple />
                    <button className="btn FileUpload-btn" onClick={this.handleSubmitFile}>Upload</button>
                </div>
                { this.state.error && <p>Somehing went wrong! Error code: {this.state.error}</p>}
                { this.state.msg && <p>{this.state.msg}</p>}
            </div>
        );
    }
}

export default FileUpload;