import React, { Component } from 'react';

import chemaxon from '../../chemaxon.svg';
import './ListFiles.css';
import '../../index.css';

class ListFiles extends Component {

    apiURL = `http://localhost:3000/api`;
    offset = 2;

    constructor(props) {
        super(props);
   
        this.state = {
            url: '',
            copied: false,
            pageNum: 0,
            files: [],
            chunkedFiles: [],
            isDataLoaded: false
        };
    }

    componentDidMount() {
        this.fetchFiles()
            .then((res) => res.json())
            .then(files => {
                this.setState({
                    files: files,
                    chunkedFiles: files.slice(0, this.offset),
                    isDataLoaded: true
                });
            });
    }

    fetchFiles() {
        return fetch(`${this.apiURL}/files`);
    }

    signedLink = file => {
        fetch(`${this.apiURL}/signed`, {
            method: 'POST',
            body: JSON.stringify({ fileName: file }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((res) => res.json())
        .then(url => {
            this.setState({
                url: url.signedUrl
            })
        });
    }

    deleteFile = (file) => {
        fetch(`${this.apiURL}/files`, {
            method: 'DELETE',
            body: JSON.stringify({ fileName: file }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then((res) => res.json())
        .then(res => {
            this.fetchFiles()
                .then((res) => res.json())
                .then(files => {
                    this.setState({
                        files: files,
                        chunkedFiles: files.slice(0, this.offset),
                        pageNum: 0,
                        isDataLoaded: true
                    });
                });
        });
    }


    nextPage() {
        let { pageNum, files } = this.state;
        ++pageNum;
        let result = 0;
        result = pageNum % 2 === 0 ? pageNum + this.offset : pageNum + 1;
        this.setState({
            pageNum: pageNum,
            chunkedFiles: files.slice(result, result + this.offset)
        });
    }

    previousPage() {
        let { pageNum, files } = this.state;
        --pageNum;
        let result = 0;
        result = pageNum % 2 === 0 ? pageNum + this.offset : pageNum + 1;
        this.setState({
            pageNum: pageNum,
            chunkedFiles: files.slice(pageNum === 0 ? 0 : result, pageNum === 0 ? this.offset : result + this.offset)
        });
    }

    formatDate(date) {
        const d = new Date(date);
        return `${d.getFullYear()}.${d.getMonth()}.${d.getUTCDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`;
    }

    render() {
        const { isDataLoaded, chunkedFiles } = this.state;
        if (!isDataLoaded) return <div>
            <img src={chemaxon} className="App-logo" alt="logo" /> </div> ;
        if (isDataLoaded && chunkedFiles.length === 0) return <div>
            <h1> No uploaded files </h1> </div> ;
        return (
            <div id="list-files">
                <h1> Uploaded files </h1>  {
                    chunkedFiles.map(file => (
                        <div className="ListFiles-table" key = { file.Key } >
                            <div className="ListFiles-name-container">
                                <p className="ListFiles-name-text"><strong>{ file.Key }</strong></p>
                                <i>uploaded: { this.formatDate(file.LastModified) } </i>
                            </div> 
                            <div>
                                <button className="btn ListFiles-btn-link" onClick={() => this.signedLink(file.Key)}>Get link</button>
                                <button className="btn ListFiles-btn-delete" onClick={() => this.deleteFile(file.Key)}>Delete </button>
                            </div>
                        </div>
                    ))
                }
                { this.state.url && <div className="ListFiles-copy-link-container"><p><strong>Link: </strong></p> <p className="ListFiles-copy-link">{this.state.url}</p></div>}
                {
                    this.state.pageNum > 0 &&
                    <button className="btn ListFiles-btn-page" onClick={() => this.previousPage()}>Previous page </button>
                }
                {
                    this.state.pageNum + this.offset <= this.state.files.length - this.offset && 
                    <button className="btn ListFiles-btn-page" onClick={() => this.nextPage()}>Next page</button>
                }
                ({ `${this.state.pageNum + 1} / ${this.state.files.length % this.offset === 0 ? this.state.files.length/this.offset : this.state.files.length - this.offset}` })
            </div>
        );
    }
}

export default ListFiles;