import React from "react";

import { useAuth0 } from "@auth0/auth0-react";

import './LoginButton.css';


const LoginButton = () => {
  const { loginWithRedirect } = useAuth0();

  
  return (
    <div className="LoginButton-container">
      Auth0 user authentication
      <button className="btn" onClick={() => loginWithRedirect()}>Log In</button>
    </div>
  )
};

export default LoginButton;