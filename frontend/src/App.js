import React from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
  NavLink,
} from "react-router-dom";

import { useAuth0 } from "@auth0/auth0-react";

import FileUpload from './components/file-upload/FileUpload';
import ListFiles from './components/file-list/ListFiles';
import LoginButton from './components/auth/login/LoginButton';
import LogoutButton from './components/auth/logout/LogoutButton';
import Profile from './components/auth/profile/Profile';
import './App.css';


function App() {
  const { isAuthenticated } = useAuth0();
  if (!isAuthenticated) return <LoginButton/>
  return (
    <div className="App-container">
      <BrowserRouter>
          <nav>
            <div className="App-nav">
              <div className="App-nav-element">
                <NavLink className={({ isActive }) => isActive ? "App-nav-link is-active" : "App-nav-link"} to='/'>Files</NavLink>
              </div>
              <div className="App-nav-element">
                <NavLink className={({ isActive }) => isActive ? "App-nav-link is-active" : "App-nav-link"} to='/upload'>Upload</NavLink>
              </div>
              <Profile/>
              <LogoutButton/>
            </div>
          </nav>
          <Routes>
            <Route path="/" element={<ListFiles />} />
            <Route path="/upload" element={<FileUpload />} />
          </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;